import socket
import threading
import queue
import time
from optparse import OptionParser

# Setup program flags and descriptions
parser = OptionParser()
parser.add_option("-d", "--debug", action="store_true", dest="debug", help="put server in debug mode where it is intentionally slowed down")
parser.add_option("-p","--port", action="store", type="int", dest="pnumber", metavar="PORT NUMBER", help="set custom port number, defaults to 8080")
parser.add_option("-i","--IP", action="store", type="string", dest="hnumber", metavar="HOST IP", help="set custom host IP, defaults to empty string")
(options, args) = parser.parse_args()

if(not options.pnumber and (len(args) >0)):
    parser.error("incorrect args\nType -h or --help for help.")

if(options.hnumber):
    host = options.hnumber
else:
    host = ''
if(options.pnumber):
    port = options.pnumber
else:
    port = 8000


serverstatus = ":)"
queue = queue.Queue()
rooms = dict() # room list with usernames connected to those rooms
aliases = dict() # dictionary with username: connection mapping
roomref = []
joinid = []

def sendMessage(username, room, message):
    roomname = roomref[int(room)]
    if username not in rooms[roomname]:
        connection = aliases[users]
        connection.sendall("ERROR_CODE: 401 ERROR_DESCRIPTION: Not in requested room, please join before attempting to send message")
        return

    roommem = rooms.pop(roomname)

    for users in roommem:
        connection = aliases[users]
        connection.sendall(("CHAT: %s CLIENT_NAME: %s MESSAGE: %s " % (room, username, message)).encode())


def aliasHelper(username, connection):
    if username not in aliases:
        aliases[username] = connection
        joinid.append(username)
        return True
    else:
        return False

def roomHelper(username, room, mode):

    # Joining
    if (mode == 0):
        if room in rooms:
            rooms[room] = rooms[room] + [username]
        else:
            rooms[room] = [username]
            roomref.append(room)
    # Leaving
    elif (mode == 1):
        roomname = roomref[int(room)]
        if roomname in rooms:
            newlist = rooms.pop(roomname)
            newlist.remove(username)
            rooms[roomname] = newlist

    # Leave all rooms when disconnecting
    elif (mode == 2):
        for aroom in rooms:
            if username in rooms[aroom]:
                newlist = rooms.pop(aroom)
                newlist.remove(username)
                rooms[aroom] = newlist
                print("Left room ",aroom)
            else:
                print("Not in any rooms")


class ThreadClients (threading.Thread):
    """Handles connected clients concurrently"""
    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue

    def run(self):
        global serverstatus
        connection = self.queue.get()
        print("doing stuff")
        while (serverstatus == ":)"):
            try:
                data = connection[0].recv(1024).decode()
            except: break

            if "HELO" in data:
                print ("*** Sending message to ", connection[1][0])
                passedText = data.split("HELO",1)[1].split("\n",1)[0].strip()
                textToSend = ("HELO "+passedText+"\nIP:"+host+"\nPort:"+str(port)+"\nStudentID:11424478").encode()
                connection[0].sendall(textToSend)

            elif "KILL_SERVICE" in data:
                print ("*** Kill request recieved from ", connection[1][0])
                serverstatus = ":("
                connection[0].sendall(("Server terminating\n").encode())
                break


            elif all(tokens in data for tokens in ["JOIN_CHATROOM:" , "CLIENT_IP:" , "PORT:" , "CLIENT_NAME:"]):
                chatroom = data.split("JOIN_CHATROOM:",1)[1].split("CLIENT",1)[0].strip()
                username = data.split("CLIENT_NAME:",1)[1].split("\n",1)[0].strip()
                if(aliasHelper(username, connection[0])):
                    roomHelper(username, chatroom, 0)
                    connection[0].sendall(("JOINED_CHATROOM: %s SERVER_IP: %s PORT: %s ROOM_REF: %s JOIN_REF: %s\n" % (chatroom, host, port, len(roomref)-1,len(joinid)-1)).encode())
                    self.queue.task_done()
                else:
                    connection[0].sendall(("ERROR_CODE: 409 ERROR_DESCRIPTION: Username already exists, please choose another\n").encode())
                    break


            elif all(tokens in data for tokens in ["LEAVE_CHATROOM:" , "JOIN_ID:" , "CLIENT_NAME:"]):
                ref = data.split("LEAVE_CHATROOM:",1)[1].split("JOIN",1)[0].strip()
                joinID = data.split("JOIN_ID:",1)[1].split("CLIENT",1)[0].strip()
                client = data.split("CLIENT_NAME:",1)[1].split("\n",1)[0].strip()
                roomHelper(client, ref, 1)
                connection[0].sendall(("LEFT_CHATROOM: %s JOIN_ID: %s\n" % (ref, joinID)).encode())


            elif all(tokens in data for tokens in ["CHAT:" , "JOIN_ID:" , "CLIENT_NAME:" , "MESSAGE:"]):
                ref = data.split("CHAT:",1)[1].split("JOIN",1)[0].strip()
                joinID = data.split("JOIN_ID:",1)[1].split("CLIENT",1)[0].strip()
                client = data.split("CLIENT_NAME:",1)[1].split("MESSAGE:",1)[0].strip()
                message = data.split("MESSAGE:",1)[1].split("\n\n:",1)[0].strip()
                sendMessage(client,ref,message)


            elif all(tokens in data for tokens in ["DISCONNECT:" , "PORT:" , "CLIENT_NAME:"]):
                client = data.split("CLIENT_NAME:",1)[1].split("\n",1)[0].strip()
                del aliases[client]
                roomHelper(client, -1, 2)
                print("Client %s disconnected" % (client))
                connection[0].close()

            else:
                print ("*** Malformed request from", connection[1][0])
                connection[0].sendall(("ERROR: Malformed request\n").encode())

            if (options.debug): time.sleep(2)
            if (data == ''): break

        #self.queue.task_done()

def startThread(queue):
    thread = ThreadClients(queue)
    thread.setDaemon(True)
    thread.start()

def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    s.bind((host, port))
    s.listen(5)
    for i in range(5):
        startThread(queue)

    while True:
        print("\t\t"+serverstatus)
        if(serverstatus == ":("): break
        conn, addr = s.accept()
        print('Connected by ', addr, queue.qsize())
        queue.put((conn,addr)) # tuple in form of (socket connection, (client address))
        queue.join()
        startThread(queue)

    print("Exiting");
    s.close()

main()
