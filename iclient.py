##
##  INTERACTIVE CLIENT
##

import socket
import time
from optparse import OptionParser


# Setup program flags and descriptions
parser = OptionParser()

parser.add_option("-p","--port", action="store", type="int", dest="pnumber", metavar="PORT NUMBER", help="set custom port number")
parser.add_option("-f","--file", action="store", type="string", dest="filename", metavar="FILENAME", help="use custom file")
parser.add_option("-m","--message", action="store", type="string", dest="message", metavar="MESSAGE", help="change default message to custom message")
parser.add_option("-k","--kill", action="store_true", dest="kill", help="send kill message to server")
(options, args) = parser.parse_args()

if(not options.pnumber and  not options.filename and not options.message and (len(args) >0)):
    parser.error("incorrect args\nType -h or --help for help.")

if(options.pnumber):
    port = options.pnumber
else:
    port = 8000

if(options.filename):
    path = options.filename
else:
    path = "/echo.php"


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = 'localhost'

try:
    s.connect((host,port))
    s.setblocking(0)

    stayAlive = True
    username = ''
    while(stayAlive):

        menu = input("What do you want to do?\n\t0. Set Username 1. Send Message\n\t2. Join Chatroom\n\t3. Leave Chatroom\n\t4. Disconnect\n\t5. Kill Server\n>")
        if(menu):
            if(menu == '0'):
                username = input("New username: ")

            #elif(menu == '1'):

            #elif(menu == '2'):

            #elif(menu == '3'):

            #elif(menu == '4'):

            #elif(menu == '5'):

        response = s.recv(1024)
        if(response):
            print(response.decode())

except:
    print("soz no")
